import axios from 'axios';
import { AUTH_SIGN_UP, AUTH_ERROR } from './types';

export const signUp = data => {
      /*
         step 1) use the data and to make the HTTP request to our BE
         step 2) take the BE response (jwtToken is here)
         step 3) Dispatch user just signed up ( with jwt)
         step 4) Save the jwtToken into our localStorage
        */
    return async dispatch => {
       try{
           const res = await axios.post('http://localhost:5000/user/signup', data);
           console.log('res', res);
           dispatch({
               type: AUTH_SIGN_UP,
               payload: res.data.token
           });

           localStorage.setItem('JWT_TOKEN', res.data.token)

       }catch(err){
           dispatch({
               type: AUTH_ERROR,
               payload: 'Email already exists. Try another one.'
           })
           console.error('err', err);
       }
    }
}