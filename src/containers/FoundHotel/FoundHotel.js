import React, { Component } from 'react';

import Hotels from '../../components/Hotels/Hotels';
import HotelsData from '../../data/hotels';
import classes from './FoundHotel.css';
import HotelSearch from '../../components/SearchBox/AdditionalSearch/HotelSearch2/HotelSearch2';
import HotelFilter from '../../components/HotelFilter/HotelFilter';

export default class FoundFlight extends Component {
                 state = {
                   hotels: HotelsData,
                   value: 0,
                   rating: false,
                   amenites: false,
                   price: false
                   
                 };
                 RatingHandler = () => {
                   this.setState({
                     rating: !this.state.rating,
                     amenites: false,
                     price: false
                   });
                 };
                 AmenitesHandler = () => {
                    this.setState({
                      amenites: !this.state.amenites,
                      rating: false,
                      price: false
                    });
                    console.log(this.state.amenites);
                 };
                 PriceHandler = () => {
                     this.setState({
                       price: !this.state.price,
                       rating: false,
                       amenites: false
                     });
                 };
                
      
                 OnInputHandler = () => {};
                 render() {
                   return (
                     <div className={classes.FoundHotel}>
                       <div className={classes.SearchBox}>
                         <HotelSearch />
                       </div>
                       <div className={classes.HotelFilter}>
                         <HotelFilter
                           rating={this.state.rating}
                           amenites={this.state.amenites}
                           price={this.state.price}
                           PriceHandler={this.PriceHandler}
                           RatingHandler={this.RatingHandler}
                           AmenitesHandler={this.AmenitesHandler}
                         />
                       </div>
                       <div className={classes.HotelsContainer}>
                         <div className={classes.MapFilter}>
                           <div className={classes.Map}>

                           </div>
                           <div className={classes.Filter}>

                           </div>
                         </div>
                         <div className={classes.Hotels}>
                           <Hotels hotels={this.state.hotels} />
                         </div>
                       </div>
                     </div>
                   );
                 }
               };