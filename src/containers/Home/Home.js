import React from 'react';

import SearchBox from '../../components/SearchBox/SearchBox';
import Destination from '../DestinationsContainer/DestinationsContainer';
import Packges from '../PackagesContainer/PackagesContainer';


import classes from './Home.css';

const Home = () => (
  <div>
    <Destination />
    <Packges />
  </div>
);

export default Home;