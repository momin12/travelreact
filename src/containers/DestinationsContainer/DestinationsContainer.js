import React, { Component } from "react";

import DestinationsData from '../../data/destinations';
import Destinations from '../../components/Destinations/Destinations';

import classes from "./DestinationsConatiner.css";

export default class DestinationsContainer extends Component {
    state = {
    destinations: DestinationsData
    };
    render() {
    return (
    <div className={classes.DestinationContainer}>
    <h3>Popular destinations</h3>
    <Destinations destinations={this.state.destinations} />
    </div>
    );
  }
};
