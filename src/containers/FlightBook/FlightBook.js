import React, {Component} from 'react';
import {Route, Switch } from 'react-router-dom';

import classes from './FlightBook.css';
import FlightLayout from '../../components/FlightLayout/FlightLayout';
import FoundFlight from '../FoundFlight/FoundFlight';
import PassengerDetails from '../../components/FlightLayout/PassengerDetails/PassengerDetails';
import Payment from '../../components/FlightLayout/Payment/Payment';
import Confirmation from '../../components/FlightLayout/Confirmation/Confirmation';

class flightBook extends Component{
    state={
        passenger: 5
    }
    render(){
         return (
           <div className={classes.FlightBook}>
             <FlightLayout>
               <Switch>
                 <Route path="/flights" exact component={FoundFlight} />
                 <Route
                   path="/flights/passenger-details"
                   exact
                   component={PassengerDetails}
                 />
                 <Route path="/payment" component={Payment} />
                 <Route path="/confirm" component={Confirmation} />
               </Switch>
             </FlightLayout>
           </div>
         );
    }
} 
export default flightBook;