import React, { Component } from 'react';

import FlightSearch from '../../components/SearchBox/AdditionalSearch/FlightSearch/FlightSearch';
import classes from './FlightContainer.css';
import Background from '../../assets/travel.jpg';

export default class FlightContainer extends Component {
      state = {
        imageHeight: null,
        imageWidth: null
      };
      componentDidMount() {
        this.setImageSize();
      }

      setImageSize = () => {
        if (window.innerWidth < 768) {
          this.setState({ imageWidth: "100%", imageHeight: 200 });
        } else {
          this.setState({ imageWidth: "100%", imageHeight: 350 });
        }
      };
      render() {
        const sectionStyle = {
          width: this.state.imageWidth,
          height: this.state.imageHeight,
          backgroundImage: `url(${Background})`,
          backgroundSize: "cover",
          borderRadius: 15
        };
        return (
          <div className={classes.FlightContainer}>
            <div style={sectionStyle}>
              <div className={classes.FlightSearchContainer}></div>
              <FlightSearch />
            </div>
          </div>
        );
      }
    };