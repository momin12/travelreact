import React, { Component } from "react";

import classes from './HotelContainer.css';
import HotelSearch from '../../components/HotelSearch/HotelSearch';
import Background from '../../assets/hotel.jpg'
import Destinations from '../DestinationsContainer/DestinationsContainer';
import Packages from '../PackagesContainer/PackagesContainer';
import DatePicker from '../../components/DatePicker/DatePicker';

export default class HotelContainer extends Component {
  state = {
        imageHeight: null,
        imageWidth: null
      };
      componentDidMount() {
        this.setImageSize();
      }

      setImageSize = () => {
        if (window.innerWidth < 768) {
          this.setState({ imageWidth: "100%", imageHeight: 200 });
        } else {
          this.setState({ imageWidth: "100%", imageHeight: 350 });
        }
      };
      render() {
        const sectionStyle = {
          width: this.state.imageWidth,
          height: this.state.imageHeight,
          backgroundImage: `url(${Background})`,
          backgroundSize: "cover",
          borderRadius: 15
        };
        return (
          <div className={classes.HotelContainer}>
            <div style={sectionStyle}>
              <div className={classes.HotelSearchContainer}></div>
              <HotelSearch />
            </div>
            <div>
              <Destinations />
            </div>
            <div>
               <Packages />
            </div>
            <div>
            </div>
          </div>
        );
      }
    };
