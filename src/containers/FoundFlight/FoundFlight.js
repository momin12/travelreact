import React, { Component } from 'react'

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faExclamationCircle,
  faAngleDown,
  faLongArrowAltRight,
  faChevronUp,
  faChevronDown
} from "@fortawesome/free-solid-svg-icons";


import classes from './FoundFlight.css';
import FlightSearch from '../../components/SearchBox/AdditionalSearch/FlightSearch2/FlightSearch2';
import Flights from '../../components/Flights/Flights';

import FlightsData from '../../data/flights';

export default class FoundFlight extends Component{
    state = {
       flights: FlightsData, 
       filterShow: false
    }

    SortHandler = () => {
      this.setState({
        filterShow : !this.state.filterShow
      })
    }
    render(){
        let SortClass;
        if(this.state.filterShow){
          SortClass = classes.SortFilter;
        }else{
          SortClass = classes.FlightNone
        }
        return (
          <div className={classes.FoundFlight}>
            <div className={classes.FlightSearch}>
              <FlightSearch />
            </div>
            <div className={classes.FlightMain}>
              <div className={classes.FilterArea}></div>
              <div className={classes.FlightAll}>
                <div className={classes.FligtsContainer}>
                  <div className={classes.FlightSort}>
                    <div className={classes.SortHeader}>
                      <h2>
                        Best departing flights{" "}
                        <span className={classes.Span}>
                          <FontAwesomeIcon icon={faExclamationCircle} />
                        </span>
                      </h2>
                      <h5>
                        Dhaka <FontAwesomeIcon icon={faLongArrowAltRight} />{" "}
                        London
                      </h5>
                      <div className={classes.FlightHighLight}>
                        <div className={classes.DateDetails}>
                          <p>
                            Departure : <span>Sat 16 November, 2019</span>
                          </p>
                          <p>
                            Return : <span>Mon 18 November, 2019</span>
                          </p>
                        </div>
                        <div className={classes.TravellerDetails}>
                          <p>
                            Travelers : <span> 2</span>
                          </p>
                          <p>
                            Length of travel : <span>2 nights</span>
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className={classes.SortList}>
                      <p onClick={this.SortHandler}>
                        Sort by{" "}
                        <span>
                          {" "}
                          <FontAwesomeIcon
                            icon={
                              this.state.filterShow
                                ? faChevronUp
                                : faChevronDown
                            }
                          />
                        </span>
                      </p>
                    </div>
                  </div>
                  <div className={SortClass}>
                    <button>Recommended flights</button>
                    <button>Campain offer</button>
                    <button>Chepest price</button>
                    <button>Shortest journey time</button>
                  </div>

                  <div>
                    <Flights flights={this.state.flights} />
                    <div className={classes.MoreFlights}>
                      <button>
                        <span>
                          <FontAwesomeIcon icon={faAngleDown} />
                        </span>
                        43 more expensive flights
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
    }
};