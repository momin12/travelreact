import React, { Component } from 'react';

import PackageData from '../../data/packages';
import Packages from '../../components/Packages/Packages';
import classes from  './PackagesContainer.css';

export default class PackageContainer extends Component{
    state={
        packages: PackageData
    }
    render(){
        return(
            <div className={classes.PackageContainer}>
                <h3>Packages</h3>
                <Packages packages={this.state.packages} />
            </div>
        );
    }
};