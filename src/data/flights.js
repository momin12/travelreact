import img1 from '../assets/biman.png';
import img2 from '../assets/bankok.png';
const flights = [
  {
    id: 1,
    from: {
      airport: "Sylhet(ZYL)",
      time: "Sat 16 November, 2019"
    },
    to: {
      airport: "Suvarnabhumi Airport (BKK)",
      time: "Mon 18 November, 2019"
    },
    stops: "Non-stop",
    travelTime: "2 h 40 m",
    type: "economy",
    flightType: "one-way",
    airlines: {
      logo: img1,
      name: "Biman",
      airNo: "being776"
    },
    price: 1045
  },
  {
    id: 2,
    from: {
      airport: "Sylhet(ZYL)",
      time: "Sat 16 November, 2019"
    },
    to: {
      airport: "Suvarnabhumi Airport (BKK)",
      time: "Sat 16 November, 2019"
    },
    stops: "1 stop",
    travelTime: "2 h 40 m",
    type: "economy",
    flightType: "one-way",
    airlines: {
      logo: img2,
      name: "Bankok airways",
      airNo: "being776"
    },
    price: 805
  },
  {
    id: 3,
    from: {
      airport: "Sylhet(ZYL)",
      time: "Sat 16 November, 2019"
    },
    to: {
      airport: "Suvarnabhumi Airport (BKK)",
      time: "Sat 16 November, 2019"
    },
    stops: "1 stop",
    travelTime: "2 h 40 m",
    type: "economy",
    flightType: "one-way",
    airlines: {
      logo: img2,
      name: "Bankok airways",
      airNo: "being776"
    },
    price: 805
  },
  {
    id: 4,
    from: {
      airport: "Sylhet(ZYL)",
      time: new Date().toISOString()
    },
    to: {
      airport: "Suvarnabhumi Airport (BKK)",
      time: "Sat 16 November, 2019"
    },
    stops: "1 stop",
    travelTime: "2 h 40 m",
    type: "economy",
    flightType: "one-way",
    airlines: {
      logo: img2,
      name: "Bankok airways",
      airNo: "being776"
    },
    price: 805
  },
  {
    id: 5,
    from: {
      airport: "Sylhet(ZYL)",
      time: "Sat 16 November, 2019"
    },
    to: {
      airport: "Suvarnabhumi Airport (BKK)",
      time: "Sat 16 November, 2019"
    },
    stops: "1 stop",
    travelTime: "2 h 40 m",
    type: "economy",
    flightType: "one-way",
    airlines: {
      logo: img2,
      name: "Bankok airways",
      airNo: "being776"
    },
    price: 805
  }
];

export default flights;