import img1 from '../assets/h1.jpg';
import img2 from "../assets/h2.jpg";
import img3 from "../assets/h3.jpg";
import img4 from "../assets/h4.jpg";
import img5 from "../assets/h5.jpg";

const hotels = [
  {
    id: 1,
    name: "Hotel Sweet Dream",
    hotelType: "5 star",
    description:
      "Warm rooms & suites in a casual hotel with rooftop garden, dining and karoke, plus free breakfast",
    price: 230,
    img: img1,
    rating: 3.9,
    location: "Dhaka"
  },
  {
    id: 2,
    name: "Hotel Salimar international",
    hotelType: "5 star",
    description:
      "Warm rooms & suites in a casual hotel with rooftop garden, dining and karoke, plus free breakfast",
    price: 230,
    img: img2,
    rating: 4.9,
    location: "Dhaka"
  },
  {
    id: 3,
    name: "Gulshan Star",
    hotelType: "5 star",
    description:
      "Warm rooms & suites in a casual hotel with rooftop garden, dining and karoke, plus free breakfast",
    price: 230,
    img: img3,
    rating: 3.9,
    location: "Dhaka"
  },
  {
    id: 4,
    name: "Hotel Sweet Dream",
    hotelType: "5 star",
    description:
      "Warm rooms & suites in a casual hotel with rooftop garden, dining and karoke, plus free breakfast",
    price: 230,
    img: img4,
    rating: 3.9,
    location: "Dhaka"
  },
  {
    id: 5,
    name: "Hotel Sweet Dream",
    hotelType: "5 star",
    description:
      "Warm rooms & suites in a casual hotel with rooftop garden, dining and karoke, plus free breakfast",
    price: 230,
    img: img5,
    rating: 3.9,
    location: "Dhaka"
  }
];

export default hotels;