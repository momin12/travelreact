import img1 from "../assets/san.jpg";
import img2 from "../assets/rome.jpg";
import img3 from "../assets/london.jpg";
import img4 from "../assets/toronto.png";

const packages = [
  {
    id: 1,
    name: "San Francisco",
    description: "Golden gate Bridge, cable cars and fog",
    img: img1
  },
  {
    id: 2,
    name: "Rome",
    description: "The Colosseum, iconic art & the Vatican",
    img: img2
  },
  {
    id: 3,
    name: "London",
    description: "Buchinghum palace",
    img: img3
  },
  {
    id: 4,
    name: "Toronto",
    description: "Super natural feelings.",
    img: img4
  },
  {
    id: 5,
    name: "San Francisco",
    description: "Golden gate Bridge, cable cars and fog",
    img: img1
  },
  {
    id: 6,
    name: "Rome",
    description: "The Colosseum, iconic art & the Vatican",
    img: img2
  },
  {
    id: 7,
    name: "London",
    description: "Buchinghum palace",
    img: img3
  },
  {
    id: 8,
    name: "Toronto",
    description: "Super natural feelings.",
    img: img4
  },
  {
    id: 9,
    name: "Toronto",
    description: "Super natural feelings.",
    img: img1
  }
];

export default packages;
