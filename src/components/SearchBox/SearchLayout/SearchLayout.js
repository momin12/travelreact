import React from "react";
import { Link } from 'react-router-dom';

import classes from "./SearchLayout.css";

const SearchLayout = props => (
  <div className={classes.SearchLayout}>
    <div className={classes.SearchNav}>
      <nav className={classes.Nav}>
        <Link to="/">Flight</Link>
        <Link to="/hotel">Hotel</Link>
      </nav>
    </div>
    {props.children}
  </div>
);

export default SearchLayout;
