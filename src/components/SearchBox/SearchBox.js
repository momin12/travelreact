import React from 'react';
import {Route, Switch} from 'react-router-dom';

import classes from './SearchBox.css';

import Searchlayout from './SearchLayout/SearchLayout';
import FlightSearch from './FlightSearch/FlightSearch';
import HotelSearch from './HotelSearch/Hotelsearch';


const SearchBox = () =>{
    return (
        <div className={classes.SearchBox}>
          <Searchlayout>
            <Switch>
              <Route path="/" exact component={FlightSearch} />
              <Route path="/hotel" component={HotelSearch} />
            </Switch>
          </Searchlayout>
        </div>
    );

}
export default SearchBox;