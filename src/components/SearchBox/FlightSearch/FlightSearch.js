import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronUp } from "@fortawesome/free-solid-svg-icons";
import { faChevronDown } from "@fortawesome/free-solid-svg-icons";

import classes from './FlightSearch.css';

export default class FlightSearch extends Component {
                 state = {
                   adult: 1,
                   children: 0,
                   show: false
                 };

                 PassengerControl = () => {
                   this.setState({
                     show: !this.state.show
                   });
                 };
                 AdultPlusController = () => {
                   this.setState({
                     adult: this.state.adult + 1
                   });
                 };
                 AdultMinusController = () => {
                   if (this.state.adult > 1) {
                     this.setState({
                       adult: this.state.adult - 1
                     });
                   } else {
                     return;
                   }
                 };
                 ChildrenPlusController = () => {
                   this.setState({
                     children: this.state.children + 1
                   });
                 };
                 ChildrenMinusController = () => {
                   if (this.state.children > 0) {
                     this.setState({
                       children: this.state.children - 1
                     });
                   }
                 };
                 SaveChangeHandler = () => {
                   this.setState({
                     show: false
                   });
                 };
                 ClearChangeHandler = () => {
                   this.setState({
                     children: 0,
                     adult: 1,
                   });
                 };
                 render() {
                   let down = (
                     <span className={classes.DropDown}>
                       <FontAwesomeIcon icon={faChevronDown} />
                     </span>
                   );
                   let up = (
                     <span className={classes.DropDown}>
                       <FontAwesomeIcon icon={faChevronUp} />
                     </span>
                   );
                   let PassengerCount;
                   if (this.state.show) {
                     PassengerCount = classes.PassengerCount;
                   } else {
                     PassengerCount = classes.PassengerCount2;
                   }
                   return (
                     <div className={classes.FlightSearch}>
                       <form className={classes.FlightForm}>
                         <div className={classes.FlightOutlet}>
                           <select
                             name="triptype"
                             form="carform"
                             className={classes.select}
                           >
                             <option value="round">Round trip</option>
                             <option value="oneway">One-way</option>
                           </select>
                           <button
                             type="button"
                             onClick={this.PassengerControl}
                           >
                             {this.state.adult + this.state.children} Travelers{" "}
                             {this.state.show ? up : down}
                           </button>
                           <select
                             name="class"
                             form="carform"
                             className={classes.select}
                           >
                             <option value="economoy">Economoy</option>
                             <option value="business">Business</option>
                             <option value="firstclass">First Class</option>
                           </select>
                         </div>
                         <div className={PassengerCount}>
                           <div>
                             <div className={classes.Customizing}>
                               <div className={classes.Subject}>
                                 <h5>Adults</h5>
                               </div>
                               <div className={classes.Value}>
                                 <button
                                   type="button"
                                   className={classes.MinusButton}
                                   onClick={this.AdultMinusController}
                                 >
                                   -
                                 </button>
                                 <span className={classes.Result}>
                                   {this.state.adult}+
                                 </span>
                                 <button
                                   type="button"
                                   className={classes.AddButton}
                                   onClick={this.AdultPlusController}
                                 >
                                   +
                                 </button>
                               </div>
                             </div>
                             <div className={classes.Customizing}>
                               <div className={classes.Subject}>
                                 <h5>Children</h5>
                                 <p>Ages 2-12</p>
                               </div>
                               <div className={classes.Value}>
                                 <button
                                   type="button"
                                   className={classes.MinusButton}
                                   onClick={this.ChildrenMinusController}
                                 >
                                   -
                                 </button>
                                 <span className={classes.Result}>
                                   {this.state.children}+
                                 </span>
                                 <button
                                   type="button"
                                   className={classes.AddButton}
                                   onClick={this.ChildrenPlusController}
                                 >
                                   +
                                 </button>
                               </div>
                             </div>
                             <div className={classes.Control}>
                               {this.state.adult + this.state.children > 1 ? (
                                 <button
                                   type="button"
                                   className={classes.ClearButton}
                                   onClick={this.ClearChangeHandler}
                                 >
                                   Clear
                                 </button>
                               ) : null}
                               <button
                                 type="button"
                                 className={classes.SaveButton}
                                 onClick={this.SaveChangeHandler}
                               >
                                 Save
                               </button>
                             </div>
                           </div>
                         </div>
                         <div className={classes.FlightDetails}>
                           <input
                             type="text"
                             placeholder="Where from?"
                             className={classes.Input1}
                           />
                           <input
                             type="text"
                             placeholder="Where to?"
                             className={classes.Input1}
                           />
                           <input
                             type="date"
                             placeholder="When"
                             className={classes.Input1}
                           />
                           <div className={classes.SearchButtonContainer}>
                             <Link to="/flights">Search</Link>
                           </div>
                         </div>
                       </form>
                     </div>
                   );
                 }
               };