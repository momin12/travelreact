import React, { Component } from "react";
import { Link } from 'react-router-dom';

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronUp } from "@fortawesome/free-solid-svg-icons";
import { faChevronDown } from "@fortawesome/free-solid-svg-icons";

import classes from './HotelSearch.css';

export default class HotelSearch extends Component {
                 state = {
                   guest: 0,
                   adult: 1,
                   room: 1,
                   children: 0,
                   show: false
                 };
                 guestController = () => {
                   this.setState({
                     show: !this.state.show
                   });
                 };
                 AdultPlusController = () => {
                   this.setState({
                     adult: this.state.adult + 1
                   });
                   console.log(this.state.adult);
                 };
                 AdultMinusController = () => {
                   if (this.state.adult > 1){
                     this.setState({
                       adult: this.state.adult - 1
                     });
                   } else {
                     return;
                   }
                 };
                 ChildrenPlusController = () => {
                   this.setState({
                     children: this.state.children + 1
                   });
                 };
                 ChildrenMinusController = () => {
                   if(this.state.children > 0) {
                      this.setState({
                        children: this.state.children - 1
                      });
                   }
                  
                 };

                 RoomPlusController = () => {
                   this.setState({
                     room: this.state.room + 1
                   });
                 };
                 RoomMinusController = () => {
                  if(this.state.room > 1) {
                     this.setState({
                       room: this.state.room - 1
                     });
                  } 
                 };

                 SaveChangeHandler = () => {
                   this.setState({
                     show: false
                   });
                 };
                 ClearChangeHandler = () => {
                   this.setState({
                     children: 0,
                     adult: 1,
                     room: 1
                   });
                 };
                 render() {
                   let down = (
                     <span className={classes.DropDown}>
                       <FontAwesomeIcon icon={faChevronDown} />
                     </span>
                   );
                   let up = (
                     <span className={classes.DropDown}>
                       <FontAwesomeIcon icon={faChevronUp} />
                     </span>
                   );
                   let sectionStyle;
                   let ShadowColor = "rgb(243, 239, 239)";
                   if (this.state.show) {
                     sectionStyle = {
                       width: "423px",
                       height: "200px",
                       background: "white",
                       marginLeft: "1.2%",
                       border: "1px solid #eee",
                       borderRadius: "2px",
                       paddingTop: "20px",
                       position: "absolute",
                       zIndex: '1',
                       // boxShadow:`5px 10px 18px ${ShadowColor}`,
                       borderRadius: '5px',
                      color: 'rgb(71, 75, 71)'
                     };
                   } else {
                     sectionStyle = {
                       display: "none"
                     };
                   }

                   return (
                     <div className={classes.HotelSearch}>
                       <form className={classes.SearchForm}>
                         <input
                           type="text"
                           placeholder="Where to?"
                           className={classes.Input1}
                         />
                         <input
                           type="Date"
                           placeholder="Check in"
                           className={classes.Date}
                         />
                         <input
                           type="Date"
                           placeholder="Check out"
                           className={classes.Date}
                         />
                         <label>Guests</label>
                         <p onClick={this.guestController} type="button">
                           {this.state.adult + this.state.children}{" "}
                           guests
                           {this.state.show ? up : down}
                         </p>
                         <div className={classes.SearchButtonContainer}>
                           <Link to="/hotels/available">Search</Link>
                         </div>
                       </form>
                       <div style={sectionStyle}>
                         <div className={classes.Customizing}>
                           <div className={classes.Subject}>
                             <h5>Adults</h5>
                           </div>
                           <div className={classes.Value}>
                             <button
                               className={classes.MinusButton}
                               onClick={this.AdultMinusController}
                             >
                               -
                             </button>
                             <span className={classes.Result}>
                               {this.state.adult}+
                             </span>
                             <button
                               className={classes.AddButton}
                               onClick={this.AdultPlusController}
                             >
                               +
                             </button>
                           </div>
                         </div>
                         <div className={classes.Customizing}>
                           <div className={classes.Subject}>
                             <h5>Children</h5>
                             <p>Ages 2-12</p>
                           </div>
                           <div className={classes.Value}>
                             <button
                               className={classes.MinusButton}
                               onClick={this.ChildrenMinusController}
                             >
                               -
                             </button>
                             <span className={classes.Result}>
                               {this.state.children}+
                             </span>
                             <button
                               className={classes.AddButton}
                               onClick={this.ChildrenPlusController}
                             >
                               +
                             </button>
                           </div>
                         </div>
                         <div className={classes.Customizing}>
                           <div className={classes.Subject}>
                             <h5>Room</h5>
                           </div>
                           <div className={classes.Value}>
                             <button
                               className={classes.MinusButton}
                               onClick={this.RoomMinusController}
                             >
                               -
                             </button>
                             <span className={classes.Result}>
                               {this.state.room}+
                             </span>
                             <button
                               className={classes.AddButton}
                               onClick={this.RoomPlusController}
                             >
                               +
                             </button>
                           </div>
                         </div>
                         <div className={classes.Control}>
                           {this.state.adult + this.state.children > 1 ? <button className={classes.ClearButton} onClick={this.ClearChangeHandler}>Clear</button>:null}
                           <button className={classes.SaveButton} onClick={this.SaveChangeHandler}>Save</button>
                         </div>
                       </div>
                     </div>
                   );
                 }
               }
