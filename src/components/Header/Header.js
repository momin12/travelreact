import React, { Component } from 'react';

import Background from '../../assets/home2.jpg';

import classes from './Header.css';
import SearchBox from '../SearchBox/SearchBox';

export default class Header extends Component {
                 state = {
                   imageHeight: null,
                   imageWidth: null
                 };
                 componentDidMount() {
                   this.setImageSize();
                 }

                 setImageSize = () => {
                   if (window.innerWidth < 768) {
                     this.setState({ imageWidth: "100%", imageHeight: 200 });
                   } else {
                     this.setState({ imageWidth: "100%", imageHeight: "100vh"});
                   }
                 };
                 render() {
                     const sectionStyle = {
                       width: this.state.imageWidth,
                       height: this.state.imageHeight,
                       backgroundImage: `url(${Background})`,
                       backgroundSize: "cover"
                     };
                   return (
                     <div style={sectionStyle}>
                         <div className={classes.SearchDemo}></div>
                       <div className={classes.SearchBoxContainer}>
                         <SearchBox />
                       </div>
                     </div>
                   );
                 }
               }