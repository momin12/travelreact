import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCaretDown } from "@fortawesome/free-solid-svg-icons";

import { faChevronUp } from "@fortawesome/free-solid-svg-icons";
import { faChevronDown } from "@fortawesome/free-solid-svg-icons";

import classes from "./HotelFilter.css";

import Amenites from './Amenites/Amenites';
import Price from './Price/Price';
import GuestRating from "./GuestRating/GuestRating";

const HotelFilter = props => (
  <div className={classes.Filter}>
    <div>
      <button onClick={props.RatingHandler}>
        Guest rating{" "}
        <span>
          {" "}
          <FontAwesomeIcon icon={props.rating ? faChevronUp : faChevronDown} />
        </span>
      </button>
      <button onClick={props.AmenitesHandler}>
        Amenites{" "}
        <span>
          {" "}
          <FontAwesomeIcon
            icon={props.amenites ? faChevronUp : faChevronDown}
          />
        </span>{" "}
      </button>
      <button onClick={props.PriceHandler}>
        Price{" "}
        <span>
          {" "}
          <FontAwesomeIcon icon={props.price ? faChevronUp : faChevronDown} />
        </span>
      </button>
    </div>
    <div className={classes.Extrdiv}>
      {props.rating ? <GuestRating /> : null}
      {props.amenites ? <Amenites /> : null}
      {props.price ? <Price /> : null}
    </div>
  </div>
);

export default HotelFilter;
