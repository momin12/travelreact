import React from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMapMarkerAlt } from "@fortawesome/free-solid-svg-icons";

import classes from './Destination.css';
const destination = props => {
  return (
    <div className={classes.Destination}>
      <img src={props.img} alt={props.name} />
      <div className={classes.DestinationDetails}>
        <h5><span><FontAwesomeIcon icon={faMapMarkerAlt}/></span>{props.name}</h5>
        <p>{props.description}</p>
      </div>
    </div>
  );
};

export default destination;
