import React from 'react';

import Destination from './Destination/Destination';
import classes from './Destinations.css';

const destinations = (props) => {
    const destinations = props.destinations.map(destination => {
        return <Destination  
        key={destination.id}
        name={destination.name} 
        description={destination.description} 
        img={destination.img}
        />
    })
    return (
        <div className={classes.Destinations}>
            {destinations}
        </div>
    )
};

export default destinations;