import React, { Component } from  'react';

import Navigation from '../Navigation/Toolbar/Toolbar';
import classes from './Layout.css'

export default class layout extends Component {
    render(){
        return (
          <div>
            <div className={classes.Nav}>
              <Navigation />
            </div>
            <main>{this.props.children}</main>
          </div>
        );
    }
}