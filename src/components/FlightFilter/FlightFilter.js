import React from 'react';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCaretDown } from "@fortawesome/free-solid-svg-icons";

import classes from './FlightFilter.css';

const flightFilter = props => (
  <div className={classes.Filter}>
    <button>
      Bags <FontAwesomeIcon icon={faCaretDown} />
    </button>
    <button>
      Stops <FontAwesomeIcon icon={faCaretDown} />
    </button>
    <button>
      Airlines <FontAwesomeIcon icon={faCaretDown} />
    </button>
    <button>
      Price <FontAwesomeIcon icon={faCaretDown} />
    </button>
    <button>
      Times <FontAwesomeIcon icon={faCaretDown} />
    </button>
    <button>
      Connecting airports <FontAwesomeIcon icon={faCaretDown} />
    </button>
    <button>
      More <FontAwesomeIcon icon={faCaretDown} />
    </button>
  </div>
);

export default flightFilter;
