import React from 'react';
import {Link} from 'react-router-dom'

import classes from './Flight.css';

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
 faChevronDown,
 faChevronUp
} from "@fortawesome/free-solid-svg-icons";

const Flight = props => (
  <div className={classes.Flight}>
    <div className={classes.FlightHeader}></div>
    <div className={classes.FlightContainer}>
      <div className={classes.FlightDetails}>
        <div className={classes.FlightAbout}>
          <div className={classes.FromDetails}>
            <h4>Departure</h4>
            <p>From</p>
            <div className={classes.FromAbout}>
              <h4>{props.from.airport}</h4>
              <p>{props.from.time}</p>
            </div>
            <p>To</p>
            <div className={classes.ToAbout}>
              <h4>{props.to.airport}</h4>
              <p>{props.to.time}</p>
            </div>
            <div className={classes.TravelTime}>
              <p>
                Travel time: <span>22h 40min</span>
              </p>
            </div>
          </div>
          <div className={classes.ReturnDetails}>
            <h4>Return</h4>
            <p>No return trip, you have selected one-way.</p>
          </div>
        </div>
      </div>
      <div className={classes.FlightPrice}>
        <h3>${props.price}</h3>
        <p>Price per adult</p>
        <Link to="/flights/passenger-details">Book</Link>
      </div>
    </div>
    <div className={classes.Details}></div>
    <div className={classes.LoadDetails}>
      <button>
        <FontAwesomeIcon icon={faChevronDown} />
      </button>
    </div>
  </div>
);

export default Flight;