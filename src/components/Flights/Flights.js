import React from 'react';

import Flight from './Flight/Flight';

const flights = (props) => {
    const flights = props.flights.map(flight => {
        return <Flight 
           key={flight.id} 
           from={flight.from}
           to={flight.to}
           travelTime={flight.travelTime}
           flightType={flight.flightType}
           airlines={flight.airlines}
           stops={flight.stops}
           price={flight.price}
           />
    })
    return (
      <div>
        {flights}
      </div>
    );
};

export default flights;