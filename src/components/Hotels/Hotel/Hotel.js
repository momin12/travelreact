import React from "react";

import classes from './Hotel.css';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMapMarkerAlt } from "@fortawesome/free-solid-svg-icons";
import { faStar } from "@fortawesome/free-solid-svg-icons";

const hotel = props => {
  return (
    <div className={classes.Hotel}>
      <div className={classes.HotelImage}>
        <img src={props.img} />
      </div>
      <div className={classes.HotelDetails}>
        <div className={classes.Details}>
          <div>
            <h4>{props.name}</h4>
            <p className={classes.Location}>
              <span>
                <FontAwesomeIcon icon={faMapMarkerAlt} />
              </span>
              {props.location}
            </p>
            <p className={classes.Rating}>
              {props.rating}
              <span>
                <FontAwesomeIcon icon={faStar} />
              </span>
            </p>
            <p className={classes.Type}>{props.type} hotel</p>
          </div>
          <div className={classes.Description}>
            <p>{props.description}</p>
          </div>
        </div>
        <div className={classes.Price}>
          <p>${props.price}</p>
        </div>
        <button>View deatils</button>
      </div>
    </div>
  );
};

export default hotel;
