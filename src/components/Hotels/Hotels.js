import React from 'react';

import classes from './Hotels.css';
import Hotel from './Hotel/Hotel';

const hotels = (props) => {
    const hotels = props.hotels.map(hotel => {
        return<Hotel 
          key={hotel.id}
          name={hotel.name}
          type={hotel.hotelType}
          description={hotel.description}
          price={hotel.price}
          img={hotel.img}
          rating={hotel.rating}
          location={hotel.location}
         />
    })
    return(
        <div className={classes.Hotels}>
            {hotels}
        </div>
    );
};

export default hotels;