import React, { Component } from 'react';

export default class CustomInput extends Component{
    render(){
        const { input: { value, onChange } } = this.props;
        return(
            <div>
                <input
                  name={this.props.name}
                  placeholder={this.props.placeholder}
                  id={this.props.id}
                  type={this.props.type}
                  value={ value }
                  onChange={ onChange}
                  required
                 />
            </div>
        );
    }
}