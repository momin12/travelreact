import React from 'react';
import { Link } from 'react-router-dom';
import classes from './ResetPassword.css';

const resetPassword = () => (
  <div className={classes.Reset}>
    <h1>Enter your email, we will send reset link</h1>
    <form>
      <input type="email" placeholder="E-mail" required />
      <div className={classes.Links}>
        <Link to="/user/signin" className={classes.CancelLink}>
          Cancel
        </Link>
        <Link className={classes.SendLink}>Send link</Link>
      </div>
    </form>
  </div>
);

export default resetPassword;