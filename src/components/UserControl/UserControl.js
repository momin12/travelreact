import React from 'react';
import { Route, Switch } from 'react-router-dom';

import classes from './UserControl.css';

import Background from '../../assets/user3.jpg';
import UserLayout from './UserLayout/UserLayout';
import SignIn from './SignIn/SignIn';
import SignUp from './SignUp/SignUp';
import ResetPassword from './ResetPassword/ResetPassword';

const userControl = () => {
        const sectionStyle = {
        width:'100%',
        height: '100vh',
        backgroundImage: `url(${Background})`,
        backgroundSize: "cover"
        };
return (
  <div style={sectionStyle}>
    <div className={classes.UserDemo}></div>
    <div className={classes.User}>
      <UserLayout>
        <Switch>
          <Route path="/user/signin" exact component={SignIn} />
          <Route path="/user/signup" exact component={SignUp} />
          <Route path="/user/resetpassword" exact component={ResetPassword} />
        </Switch>
      </UserLayout>
    </div>
  </div>
);
}

     
              
    
export default userControl;