import React, { Component } from 'react';
import { reduxForm, Field} from 'redux-form';
import {Link} from 'react-router-dom';
import { connect } from 'react-redux';
import{ compose } from 'redux';
import FacebookLogIn from 'react-facebook-login';
import GoogleLogIn from 'react-google-login';


import * as actions from '../../../actions';
import classes from './SignUp.css';
import CustomInput from '../CustomInput';

export default class signUp extends Component{
    constructor(props){
        super(props);
        this.onSubmit = this.onSubmit.bind(this)
    }
    async onSubmit(formData){
        console.log('formData', formData);
        await this.props.signUp(formData)
    }
   
    responseGoogle(res){
         console.log('responseGoolge', res);
    }
    responseFacebook(res){
        console.log('responseFacebook', res);
   }
    
    render(){
        return (
          <div className={classes.Main}>
            <h4>Sign up</h4>
            <form className={classes.SignupForm}>
              <input
                type="text"
                name="name"
                onChange={this.handleChange}
                placeholder="Your name"
                required
              />
              <input
                type="email"
                name="email"
                onChange={this.handleChange}
                placeholder="E-mail"
                required
              />
              <input
                type="password"
                name="password"
                onChange={this.handleChange}
                placeholder="Password"
                required
              />
              <button type="submit" className={classes.SubmitButton}>
                Sign up
              </button>
            </form>
            <p>
              Already have an account ? <Link to="/user/signin"> Log in</Link>{" "}
            </p>
            <div className={classes.OthersSignUp}></div>
          </div>
        );
    }
};