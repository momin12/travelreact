import React from 'react';

import classes from './UserLayout.css';

const userLayout = (props) => (
    <div className={classes.UserLayout}>
        {props.children}
    </div>
);

export default userLayout;