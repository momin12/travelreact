import React, { Component } from 'react';
import { Link } from "react-router-dom";
import axios from "axios";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faGoogle } from "@fortawesome/free-solid-svg-icons";
import { faFacebookSquare } from "@fortawesome/free-solid-svg-icons";


import classes from './SignIn.css';
class LoginForm extends Component{
    state= {
        email:'',
        pasword: ''
    }

    handleChange = event => {
        const { name, value} = event.target
        this.setState({ [name]: value})
    }

    handleSubmit = event => {
          event.preventDefault();
          const  user_data = {
                email: this.state.email,
                password: this.state.password
            };

           // axios({
               // method: 'post',
              //  url:"http://localhost:5000/user/signin",
              //  data: user_data
            //}).then(result => {
                console.log(user_data)
          //  })
      
    }

    googleAutHandler = (event) => {
        event.preventDefault();
        axios({
            method: 'get',
            url:"http://localhost:5000/auth/google"
        }).then(result => {
            console.log(result.data)
        })
    }
    render(){
        return (
          <div className={classes.SignContainer}>
                <h4>Log in</h4>
                <form
                  className={classes.SigninForm}
                  onSubmit={this.handleSubmit}
                >
                  <input
                    type="email"
                    name="email"
                    onChange={this.handleChange}
                    placeholder="E-mail"
                    required
                  />
                  <input
                    type="password"
                    name="password"
                    onChange={this.handleChange}
                    placeholder="Password"
                    required
                  />
                  <button className={classes.SubmitButton} type="submit">
                    Log in
                  </button>
                  <div className={classes.Reference}>
                    <Link className={classes.Forget} to="/user/resetpassword">
                      Forgot password ?
                    </Link>
                    <p>
                      Haven't account ? <Link to="/user/signup"> Sign up</Link>
                    </p>
                  </div>
                  <p>Log in with</p>
                  <div className={classes.Icons}>
                    <button type="button" className={classes.Facebook}></button>
                    <button type="button" className={classes.Instargram}>
                      <i className="fab fa-instagram"></i>
                    </button>
                    <button
                      type="button"
                      onClick={this.googleAutHandler}
                      className={classes.Google}
                    >
                      <i className="fab fa-google"></i>
                    </button>
                  </div>
                </form>
              </div>
        );
    }
};

export default LoginForm;