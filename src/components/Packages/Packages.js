import React from 'react';

import Package from './Package/Package';
import classes from './Packages.css';

const packages = (props) => {
    const packages = props.packages.map(pack => {
        return <Package 
          key={pack.id} 
          name={pack.name}
          description={pack.description} 
          img={pack.img} />
    })
    return(
        <div className={classes.Packages}>
           { packages }
        </div>
    );
};

export default packages;