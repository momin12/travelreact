import React from 'react';

import classes from './Package.css';

const pack = (props) => {
    return (
      <div className={classes.Package}>
        <img src={props.img} alt={props.name} />
        <div className={classes.PackageDetails}>
          <h5>{props.name}</h5>
          <p>{props.description}</p>
          <button>Book now</button>
        </div>
      </div>
    );
};

export default pack;