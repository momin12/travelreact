import React from 'react';

import classes from './NavigationItems.css';

import { Link } from 'react-router-dom';

  
const NavigationItems = (props) =>{
  let nav;
  if(!props.auth){
    nav = (
      <ul className={classes.NavigationItems}>
        <li className={classes.NavigationItem}>
          <Link className={classes.ListLink} to="/">
            Flights
          </Link>
        </li>
        <li className={classes.NavigationItem}>
          <Link className={classes.ListLink} to="/hotel">
            Hotels
          </Link>
        </li>
        <li className={classes.NavigationItem}>
          <Link className={classes.ListLink} to="/packages">
            Packages
          </Link>
        </li>
        <li className={classes.NavigationItem}>
          <Link className={classes.ListLink} to="/cruise">
           Cruise
          </Link>
        </li>
        <li className={classes.NavigationItem}>
          <Link to="/user/signin" className={classes.SignLink}>
            Log in
          </Link>
        </li>
      </ul>
    );
   };
   if(props.auth){
     nav =<ul className={classes.NavigationItems}>
     <li className={classes.NavigationItem}>
       <Link to="#" className={classes.ListLink}>Orders</Link>
     </li>
     <li className={classes.NavigationItem}>
       <Link className={classes.ListLink} to="/signup">Profile</Link>
      </li>
      <li className={classes.NavigationItem}>
        <Link to="/signin" className={classes.SignLink} >Log out</Link>
      </li>
   </ul>
   }
  return(
   <div>
     {nav}
   </div>
  );
}
   

  

export default NavigationItems;