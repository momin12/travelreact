import React from 'react';

import classes from './Toolbar.css';
import NavigationItems from '../NavigationItems/NavigationItems';
import DrawerToggle from '../SideDrawer/DrawerToggle/DrawerToggle';

import Logo from '../../Logo/Logo';

import { Link } from 'react-router-dom';

const toolbar = props => (
  <header className={classes.Toolbar}>
    <DrawerToggle clicked={props.DrawerToggleClicked} />
    <div className={classes.Logo}>
      <Link to="/">
       <Logo/>
      </Link>
    </div>

    <nav className={classes.DekstopOnly}>
      <NavigationItems
        auth={props.auth}
        signup={props.signup}
        clicked={props.clicked}
      />
    </nav>
  </header>
);

export default toolbar;