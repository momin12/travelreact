import React from "react";

import classes from './PassengerDetails.css';
import ContactInformation from './ContactInformation/ContactInformation';
import Passengers from './Passengers/Passengers';

const PassengerDetails = () => (
  <div className={classes.PassengerDetails}>
    <div className={classes.Passenger}>
        <div className={classes.ContactInformation}>
            <ContactInformation />
        </div>
        <div className={classes.PassengerAll}>
             <Passengers />
        </div>
    </div>
    <div className={classes.Other}></div>
  </div>
);

export default PassengerDetails;
