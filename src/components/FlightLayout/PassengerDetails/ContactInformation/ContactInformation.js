import React from 'react';

import classes from './ContactInformation.css';

const contactInformation = () => (
  <div className={classes.ContactInformation}>
    <h3>Contact informattion for all passengers</h3>
    <input type="email" name="email" placeholder="E-mail" />
    <input type="email" name="confirm-email" placeholder="Confirm e-mail" />
    <div className={classes.OtherInformation}>
      <div className={classes.OtherInformation1}>
        <label>Country</label>
        <select>
          <option>USA</option>
          <option>UK</option>
          <option>Bangladesh</option>
          <option>India</option>
          <option>Pakistan</option>
          <option>German</option>
          <option>Singapore</option>
          <option>Netherland</option>
          <option>Japan</option>
          <option>China</option>
          <option>Uruguye</option>
          <option>Brazil</option>
          <option>Argentina</option>
          <option>Livia</option>
        </select>
      </div>
      <div className={classes.OtherInformation2}>
        <label>Mobile number</label>
        <input type="text" name="mobile" />
      </div>
    </div>
  </div>
);
export default contactInformation;