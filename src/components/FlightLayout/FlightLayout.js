import React from 'react';

import classes from './FlightLayout.css';

const flightLayout = (props) => (
    <div className={classes.FlightLayout}>
        {props.children}
    </div>
);

export default flightLayout;