import React, { Component } from 'react';

import classes from './Subscription.css';

export default class Subscription extends Component{
    render(){
        return (
          <div className={classes.Subscription}>
            <form>
              <p>
                Join our community to receive the latest deals, exclusive
                discounts & travel inspiration!
              </p>
              <div>
                  <input type="email" placeholder="Email Address" required/>
                  <button>Submit</button>
              </div>
            </form>
          </div>
        );
    }
};