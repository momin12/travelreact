import React from "react";

import Logo from "../../assets/logo.png";

import classes from "./Logo.css";

const logo = () => (
  <div className={classes.Logo}>
    <img src={Logo} alt="Fusion Travel" />
  </div>
);

export default logo;
