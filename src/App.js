import React from 'react';
import {HashRouter as Router, Route} from 'react-router-dom';

import classes from './App.css';

import Layout from './components/Layout/Layout';
import Header from './components/Header/Header';
import Home from './containers/Home/Home';
import Subscription from './components/Subscription/Subscription';
import FlightBook from './containers/FlightBook/FlightBook';
import FoundFlight from './containers/FoundFlight/FoundFlight';
import FoundHotel from './containers/FoundHotel/FoundHotel';
import UserControl from './components/UserControl/UserControl';
import Footer from './components/Footer/Footer';


function App() {
  return (
    <Router>
      <div className={classes.App}>
        <Layout>
          <Route path={["/", "/hotel"]} exact component={Header} />
          <div className={classes.AppLayout}>
            <Route path={["/", "/hotel"]} exact component={Home} />
            <Route path="/flights" component={FlightBook} />
            <Route path="/hotels/available" exact component={FoundHotel} />
          </div>
          <Route path="/user" component={UserControl} />
          <Route
            exact
            path={["/", "/hotel", "/flights"]}
            component={Subscription}
          />

          <Route
            exact
            path={["/", "/hotel", "/flights", "/hotels/available"]}
            component={Footer}
          />
        </Layout>
      </div>
    </Router>
  );
}

export default App;
